# Modular Weather Station

An ESP8266 based weather station, designed to be modular.

## Hardware requirements

This station is based on ESP8266 boards, like the NodeMCU.  I personally develop using a LoLin
mini, but also tested the station with an Arduino UNO and a NodeMCU v3 board.

### Temperature and humidity

To measure temperature and humidity, you will need a DHT11 or DHT22 sensor.  DHT11, usually blue
in color, is for indoor usage since it’s not reliable under freezing temperature, while DHT22,
usually white in color, can be used outside.

### Atmospheric pressure

To measure pressure, you will need a BMP085 or BMP180 module, connected to the main board via I²C.
The sensor on it is also capable of measuring temperature, so if you don’t have a DHT11/DHT22 at
hand, you can still get temperature data (the firmware prefers DHT11/DHT22 if present).

### Ambient light

To measure ambient light level, you will need a BH1750 module, connected to the board via I²C.

### Rain

To check if it’s raining or not, you will need a FC-37 or YL-83 module.  Since the only analogue
pin of the board is reserved for checking the battery level, you have to use the digital pin of
the module.

## Power source

If you can put the whole box near an outlet plug, you are good to go! Just attach an old USB phone
charger to the board, and your station is powered.

If that’s not an option for you, you can attach a battery to the VIN and GND pins. Consult your
board’s manual for the voltage range.

## Compiling

Copy the file `config.h.sample` to `config.h` and edit the parameters within.

You can either compile the whole sketch from within the Arduino IDE or, if you are familiar with
the command line, you can use the official [Arduino CLI
tools](https://arduino.github.io/arduino-cli/):

```
arduino-cli compile -b --fqbn esp8266:esp8266:nodemcuv2 /path/to/the/sketch /path/to/the/sketch/build
```
