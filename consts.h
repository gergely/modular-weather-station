/*
 * consts.h - Constants for the ModularWeatherStation
 *
 * Copyright (C) 2021 Gergely Polonkai
 * Author: Gergely POLONKAI <gergely@polonkai.eu>
 *
 * ModularWeatherStation is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * ModularWeatherStation is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * As additional permission under GNU GPL version 3 section 7, you may
 * distribute non-source form of the Program without the copy of the
 * GNU GPL normally required by section 4, provided you inform the
 * receipents of GNU GPL by a written offer.
 *
 */

// Prometheus Pull mode.  A Prometheus instance has to be configured to
// regularly pull values from the station.
#define MODE_PROM_PULL 1

// NOTE: Bit 1 (ie. value of 2) is reserved for MODE_PROM_PUSH when it gets
// implemented
