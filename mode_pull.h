#ifndef __PWS_MODE_PULL_H
# define __PWS_MODE_PULL_H

# include "config.h"

class PWSModePull {
  public:
    PWSModePull(int port);
    void begin();
    void loop();
    void sendValues(
#if defined(DHT_TYPE) || defined(HAVE_BMP180)
    float temperature,
#endif
#ifdef DHT_TYPE
    float humidity,
#endif
#ifdef RAIN_PIN
    bool raining,
#endif
#ifdef HAVE_BH1750
    float light,
#endif
#ifdef HAVE_BMP180
    float pressure,
#endif
    ...);
  private:
    int _port;
};
#endif // __PWS_MODE_PULL_H
