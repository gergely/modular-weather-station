#include <ESP8266WebServer.h>

#include "mode_pull.h"
#include "config.h"

#if defined(DHT_TYPE) || defined(HAVE_BMP180)
float _temperature;
#endif
#ifdef DHT_TYPE
float _humidity;
#endif
#ifdef HAVE_BH1750
float _light;
#endif
#ifdef HAVE_BMP180
float _pressure;
#endif
#ifdef RAIN_PIN
bool _raining;
#endif

static const char MIME_TEXT[] PROGMEM = "text/plain";
static const char MIME_METRICS[] PROGMEM = "text/plain; version=0.0.4; charset=utf-8";

#if defined(DHT_TYPE) || defined(HAVE_BMP180)
# define TEMPL_TEMPERATURE \
    "# HELP temperature_celsius Temperature in degrees Celsius\n" \
    "# TYPE temperature_celsius gauge\n" \
    "temperature_celsius %.2f\n"
#endif
#ifdef DHT_TYPE
# define TEMPL_HUMIDITY \
    "# HELP relative_humidity Relative humidity, in percents\n" \
    "# TYPE relative_humidity gauge\n" \
    "relative_humidity %.2f\n"
#endif
#ifdef RAIN_PIN
# define TEMPL_RAIN \
    "# HELP raining Boolean value checking if it is raining\n" \
    "# TYPE raining gauge\n" \
    "raining %d\n"
#endif
#ifdef HAVE_BH1750
# define TEMPL_LIGHT \
    "# HELP light Ambient light, in lumens\n" \
    "# TYPE light gauge\n" \
    "light %.2f\n"
#endif
#ifdef HAVE_BMP180
# define TEMPL_PRESSURE \
    "# HELP pressure Atmospheric pressure, in Pa\n" \
    "# TYPE pressure gauge\n" \
    "pressure %d\n"
#endif

ESP8266WebServer server;

static void
_replyNotFound()
{
    server.send(404, FPSTR(MIME_TEXT), "Not Found");
}

static void
_sendMetrics()
{
    size_t message_size =
#if defined(DHT_TYPE) || defined(HAVE_BMP180)
        sizeof(TEMPL_TEMPERATURE) +
#endif // defined(DHT_TYPE) || defined(HAVE_BMP180)
#ifdef DHT_TYPE
        sizeof(TEMPL_HUMIDITY) +
#endif // DHT_TYPE
#ifdef RAIN_PIN
        sizeof(TEMPL_RAIN) +
#endif // RAIN_PIN
#ifdef HAVE_BH1750
        sizeof(TEMPL_LIGHT) +
#endif // HAVE_BH1750
#ifdef HAVE_BMP180
        sizeof(TEMPL_PRESSURE) +
#endif // HAVE_BMP180
        1;
    char message[message_size];

    snprintf(message, message_size, ""
#if defined(DHT_TYPE) || defined(HAVE_BMP180)
             TEMPL_TEMPERATURE
#endif // defined(DHT_TYPE) || defined(HAVE_BMP180)
#ifdef DHT_TYPE
             TEMPL_HUMIDITY
#endif // DHT_TYPE
#ifdef RAIN_PIN
             TEMPL_RAIN
#endif // RAIN_PIN
#ifdef HAVE_BH1750
             TEMPL_LIGHT
#endif // HAVE_BH1750
#ifdef HAVE_BMP180
             TEMPL_PRESSURE
#endif // HAVE_BMP180
#if defined(DHT_TYPE) || defined(HAVE_BMP180)
             , _temperature
#endif // defined(DHT_TYPE) || defined(HAVE_BMP180)
#ifdef DHT_TYPE
             , _humidity
#endif // DHT_TYPE
#ifdef RAIN_PIN
             , _raining ? 1 : 0
#endif // RAIN_PIN
#ifdef HAVE_BH1750
             , _light_level
#endif // HAVE_BH1750
#ifdef HAVE_BMP180
             , _pressure
#endif // HAVE_BMP180
        );

    server.send(200, MIME_METRICS, message);
}

PWSModePull::PWSModePull(int port)
{
    _port = port;
}

void
PWSModePull::begin()
{
    server.on("/metrics", HTTP_GET, _sendMetrics);
    server.onNotFound(_replyNotFound);

    server.begin(_port);
}

void
PWSModePull::loop()
{
    server.handleClient();
}

void
PWSModePull::sendValues(
#if defined(DHT_TYPE) || defined(HAVE_BMP180)
    float temperature,
#endif
#ifdef DHT_TYPE
    float humidity,
#endif
#ifdef RAIN_PIN
    bool raining,
#endif
#ifdef HAVE_BH1750
    float light,
#endif
#ifdef HAVE_BMP180
    float pressure,
#endif
    ...)
{
#if defined(DHT_TYPE) || defined(HAVE_BMP180)
    _temperature = temperature;
#endif
#ifdef DHT_TYPE
    _humidity = humidity;
#endif
#ifdef RAIN_PIN
    _raining = raining;
#endif
#ifdef HAVE_BH1750
    _light = light;
#endif
#ifdef HAVE_BMP180
    _pressure = pressure;
#endif
}
